using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Mordred.Obj;
using Mordred.Obj.Char;
using Mordred.Obj.Item;
using Mordred.Game;
using Mordred.Game.SubSystem;

/**
 * Potjam #5 - Mordred
 * (c) Sebastian Schreiber 2013
 * 
 * Zentrale Steuerungsklasse fuer Spieler
 */

namespace Mordred.User
{
    [RequireComponent(typeof(Character))]
    public class Player : MonoBehaviour
    {

        public Inventory Inventory { get; private set; }
        public Character Character { get; private set; }

        // Use this for initialization
        void Start()
        {
            this.Character = GetComponent<Character>();
            this.Inventory = new Inventory(this);
        }


        void OnDrawGizmosSelected()
        {
            // Display the explosion radius when selected
            WorldObject wo = this.Inventory.GetActivItem();

            if (wo != null && wo.type == ObjectType.WEAPON)
            {
                float reach = ((Weapon)wo).distance;
                //Debug.Log("Reach:" + reach);
                Gizmos.color = Color.white;
                Gizmos.DrawWireSphere(transform.position, reach);
            }
        }

    }

}
