using UnityEngine;
using System.Collections;

using Mordred.Game.Handler;
using Mordred.Obj;
using Mordred.Obj.Char;
using Mordred.Game;

namespace Mordred.User
{
    public class UserInput : MonoBehaviour
    {

        public Player player;

        public LayerMask terrainLayerMask;
        public LayerMask usableLayerMask;
        private Vector3 center;

        // Use this for initialization
        void Start()
        {
            Debug.Log("player=" + player);
            this.player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
            Debug.Log("player=" + player);
            this.center = new Vector3(Screen.width / 2, Screen.height / 2, 0);
        }

        // Update is called once per frame
        void Update()
        {

            if (GameHandler.instance.IsGameState(GameState.PLAY))
            {
                MouseActivity();
            }
            KeyActivity();
        }

        private void MouseActivity()
        {
            //Debug.Log("Button"+Input.GetMouseButton(0)+"Down"+Input.GetMouseButtonDown(0)+"Up"+Input.GetMouseButtonUp(0));
            if (Input.GetMouseButtonDown(0))
            {
                LeftMouseClick();
            }
            else if (Input.GetMouseButton(1))
            {
                RightMouseClick();
            }
            else
            {
                animation.CrossFade("bow_idle");
            }
        }

        private void KeyActivity()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                MenuHandler.Instance.ToggleMenu();
            }
            else if (Input.GetKeyDown(KeyCode.F1))
            {
                MenuHandler.Instance.ShowMenu(MenuType.SAVELOAD);
            }
            else if (Input.GetKeyDown(KeyCode.I))
            {
                MenuHandler.Instance.ShowMenu(MenuType.INVENTORY);
            }
        }

        private void LeftMouseClick()
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, usableLayerMask))
            {
                Debug.Log("Clicked on USABLE.");
                UseCodeHandler.Instance.Handle(hit.collider.gameObject);
            }
            else
            {
                //TODO: Auf Tablets wird der Angriff durch einen button ausgeloest
                WarHandler.Instance.TryAttack();
                Debug.Log("Attack....");
                //animation.Play("1h_attack1");
            }
        }

        public void RightMouseClick()
        {
            //Debug.Log("Right Clickt");
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            //if (Physics.Raycast (ray, out hit, Mathf.Infinity, terrainLayerMask)) {
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << 8))
            {
                //print ("hit -->"+hit.collider.gameObject.name + hit.collider.gameObject.transform);
                CharacterController controller = player.GetComponent<CharacterController>();

                //Debug.Log("Diff:");     
                player.transform.LookAt(hit.point);
                player.transform.eulerAngles = new Vector3(0, player.transform.rotation.eulerAngles.y, player.transform.rotation.eulerAngles.z);

                // Calculate run or walk
                // Be aware! Resizing the screens means the center has to be recalculated.
                Vector3 direction = transform.TransformDirection(Vector3.forward);
                float dis = Vector2.Distance(Input.mousePosition, center);
                //Debug.Log("Dis:"+dis);

                string ani = dis > 200 ? "run" : "walk4";
                float speed = dis > 200 ? 7 : 3;
                animation.CrossFade(ani);

                controller.SimpleMove(direction * speed);
            }
            else
            {
                animation.Play("bow_idle");
            }

        }
    }

}
