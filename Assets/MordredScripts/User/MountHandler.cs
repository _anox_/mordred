using UnityEngine;
using System.Collections.Generic;

using Mordred.Game;

namespace Mordred.User
{

    public class MountHandler : MonoBehaviour
    {

        public Dictionary<SlotType, Transform> mountDic /*lool*/ = new Dictionary<SlotType, Transform>();
        public Transform headMount, torsoMount, rightHandMount, leftHandMount, legsMount, feetMount;


        public void Start()
        {

            mountDic.Add(SlotType.RIGHTHAND, MordredUtils.FindTransform(transform, "_rightHandMount"));
            mountDic.Add(SlotType.LEFTHAND, MordredUtils.FindTransform(transform, "_leftHandMount"));
            mountDic.Add(SlotType.HEAD, MordredUtils.FindTransform(transform, "_headMount"));
            /*mountDic.Add(SlotType.TORSO, FindTransform(transform, "_torsoMount"));
            mountDic.Add(SlotType.LEGS, FindTransform(transform, "_legsMount"));
            mountDic.Add(SlotType.FEET, FindTransform(transform, "_feetHandMount"));*/

        }

        public Transform GetSlotMount(SlotType slot)
        {
            return mountDic[slot];
        }
    }

}



