using UnityEngine;
using System.Collections;

namespace Mordred.Game
{

    [RequireComponent(typeof(SphereCollider))]
    public class AreaTrigger : MonoBehaviour
    {

        private float defaultDistance = 25F;
        public float distance;
        public enum TriggerTypeEnum { ENABLE, COLOR };
        public TriggerTypeEnum triggerType = TriggerTypeEnum.ENABLE;

        void Start()
        {
            if (distance == 0)
            {
                distance = defaultDistance;
            }

            ObjEnable(false);
            SphereCollider c = GetComponent<SphereCollider>();
            c.radius = distance;
        }

        void OnTriggerEnter(Collider c)
        {

            if (c.gameObject.tag == "Player")
            {
                //print("ENTER");
                ObjEnable(true);
            }
        }

        void OnTriggerExit(Collider c)
        {

            if (c.gameObject.tag == "Player")
            {
                //print("EXIT");
                ObjEnable(false);
            }
        }

        private void ObjEnable(bool b)
        {
            for (var i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.SetActive(b);
                //transform.GetChild(i).gameObject.SetActiveRecursively(b);
            }

        }

    }

}

