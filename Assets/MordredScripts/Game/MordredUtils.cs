using UnityEngine;

using System.Collections;
using System.Collections.Generic;


namespace Mordred.Game
{

    public class MordredUtils : MonoBehaviour
    {

        private static MordredUtils instance = null;

        public static MordredUtils Instance
        {
            get
            {
                if (instance == null)
                {
                    Debug.Log("Starte MordredUtils System.");
                    instance = GameObject.FindGameObjectWithTag("handler").AddComponent<MordredUtils>();
                }
                return instance;
            }
        }

        /** Referenzen die allen Prefabs die zur Laufzeit instanziiert werden sollen */
        private Dictionary<PrefabID, Object> prefabs = new Dictionary<PrefabID, Object>();
        public GameObject pitchfork;

        private static Quaternion q = Quaternion.identity;
        private static Vector3 v = Vector3.zero;


        public void Start()
        {
            Debug.Log("Starting Mordred Utils");

	    // Items
            prefabs.Add(PrefabID.PITCHFORK, Resources.Load("item_pitchfork"));
            prefabs.Add(PrefabID.GOLD_PILE, Resources.Load("item_goldpile"));
	    

            if (prefabs.Count < System.Enum.GetNames(typeof(PrefabID)).Length)
            {
                Debug.LogError("Achtung! Achtung!\nEs wurden mehr ItemPrefabs definiert als in Mordred zugewiesene.");
            }
        }

        public GameObject InstantiateItem(PrefabID prefab, Vector3 v)
        {

            Debug.Log("Trying to get prefab:" + prefab + " Prefab count:" + prefabs.Count);
            Debug.Log("Trying to get prefab:" + prefabs[PrefabID.PITCHFORK]);
            //if(prefabs[prefab] 
            GameObject g = (GameObject)Instantiate(prefabs[prefab], v, q);
            return g;
        }

        public GameObject InstantiateNpc(SaveNpc saveNpc)
        {
            GameObject g = (GameObject)Instantiate(prefabs[saveNpc.prefab], v, q);
            g.transform.position = SaveTools.Vec3ToVector3(saveNpc.pos);
            g.transform.eulerAngles = SaveTools.Vec3ToVector3(saveNpc.rot);

            return g;
        }


        public static Transform FindTransform(Transform parent, string name)
        {
            if (parent.name == name) return parent;

            Transform[] transforms = parent.GetComponentsInChildren<Transform>();
            foreach (Transform t in transforms)
            {
                if (t.name == name) return t;
            }
            return null;
        }

    }
}
