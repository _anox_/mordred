using UnityEngine;

using Mordred;

public class SaveTools {

	public static Vector3 Vec3ToVector3(Vec3 v3) {
		return new Vector3(v3.x, v3.y, v3.z);
	}

	public static Vec3 Vector3ToVec3(Vector3 v3) {
		return new Vec3(v3.x, v3.y, v3.z);
	}


}
