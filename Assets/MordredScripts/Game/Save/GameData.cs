using System.Collections.Generic;
using System.Collections;
using System;
using System.IO;

using Mordred.Game.Environment;

namespace Mordred.Game.Save
{
    [System.Serializable]
    public class GameData
    {

        public GameData() { }

        // ---------------------- Game Stuff   ----------------------
        // Date in RL 
        public DateTime saveDate { get; set; }
        public float worldTime { get; set; }
        public DayNightController.DayPhase dayPhase { get; set; }
        public Vec3 sunRot { get; set; }



        // ---------------------- Player Stuff ----------------------
        //Players name
        public string name { get; set; }

        // Player pos + rot
        public Vec3 pos { get; set; }
        public Vec3 rot { get; set; }

        public SaveInvItem[] invItems { get; set; }
        public SaveNpc[] npcList { get; set; }

    }
}