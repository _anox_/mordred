using System.Collections.Generic;

using UnityEngine;
using System.Collections;
//You must include these namespaces
//to use BinaryFormatter
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

using Mordred.User;
using Mordred.Game.Handler;
using Mordred.Game.Environment;

namespace Mordred.Game.Save
{
    public class SaveLoad : MonoBehaviour
    {

        private static string savegameName = "MordedSave_";
        private static Dictionary<int, GameData> saveGames = new Dictionary<int, GameData>();
        public static int SLOTS = 6;
        private static Player player;

        public static void SaveGameData(int sgIndex)
        {
            Debug.Log("Saving game data...");
            GameData data = CollectCurrentData();

            //Get a binary formatter
            var b = new BinaryFormatter();
            //Create an in memory stream
            var m = new MemoryStream();
            //Save the scores
            b.Serialize(m, data);
            //Add it to player prefs
            PlayerPrefs.SetString(savegameName + sgIndex, Convert.ToBase64String(m.GetBuffer()));
            saveGames[sgIndex] = data;
            Debug.Log("Finished saving.....");
        }

        public static void LoadGameDataFromCache(int sgIndex)
        {
            GameData gameData = null;
            saveGames.TryGetValue(sgIndex, out gameData);
            if (gameData != null)
            {
                SetCurrentData(gameData);
            }
        }

        public static GameData LoadGameDataFromDisk(int sgIndex)
        {
            GameData gameData = null;

            //Get the data
            var dataStr = PlayerPrefs.GetString(savegameName + sgIndex);

            //If not blank then load it
            if (!string.IsNullOrEmpty(dataStr))
            {
                Debug.Log("Savegame " + sgIndex + " found. Loading...");
                //Binary formatter for loading back
                var b = new BinaryFormatter();
                //Create a memory stream with the data
                var m = new MemoryStream(Convert.FromBase64String(dataStr));
                //Load back the scores
                gameData = (GameData)b.Deserialize(m);
            }
            else
            {
                //Debug.Log("No Savegame found");
            }

            return gameData;
        }


        public static Dictionary<int, GameData> GetSaveGames()
        {
            saveGames.Clear();
            for (var i = 0; i < SLOTS; i++)
            {
                GameData gd = LoadGameDataFromDisk(i);
                saveGames.Add(i, gd);
                //Debug.Log("Savegame found:"+gd); 
            }
            return saveGames;
        }

        private static void SetCurrentData(GameData gd)
        {
            //--------------- Player stuff:
            player.Character.CharName = gd.name;
            player.transform.position = SaveTools.Vec3ToVector3(gd.pos);
            player.transform.eulerAngles = SaveTools.Vec3ToVector3(gd.rot);
            player.Inventory.LoadSavedItems(gd.invItems);

            //--------------- Game stuff:
            DayNightController.Instance.currentCycleTime = gd.worldTime;
            DayNightController.Instance.currentPhase = gd.dayPhase;
            DayNightController.Instance.theSun.eulerAngles = SaveTools.Vec3ToVector3(gd.sunRot);

            NPCHandler.Instance.LoadSavedNPCs(gd.npcList);
        }

        private static GameData CollectCurrentData()
        {
            GameData gd = new GameData();
            player = (Player)GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

            //--------------- Player stuff:
            gd.name = player.Character.CharName;
            gd.pos = SaveTools.Vector3ToVec3(player.transform.position);
            gd.rot = SaveTools.Vector3ToVec3(player.transform.eulerAngles);
            gd.invItems = player.Inventory.SaveInvItems();

            //--------------- Game stuff:
            gd.saveDate = new DateTime();
            gd.worldTime = DayNightController.Instance.currentCycleTime;
            gd.dayPhase = DayNightController.Instance.currentPhase;
            gd.sunRot = SaveTools.Vector3ToVec3(DayNightController.Instance.theSun.eulerAngles);

            gd.npcList = NPCHandler.Instance.SaveNPCs();

            return gd;
        }

    }
}
