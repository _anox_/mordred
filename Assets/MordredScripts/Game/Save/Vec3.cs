using System.Collections.Generic;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
	
[System.Serializable]
public class Vec3 {
	public Vec3() {}
	public Vec3(float x, float y, float z) { this.x=x; this.y=y; this.z=z; }
	public float x {get;set;}
	public float y {get;set;}
	public float z {get;set;}
}


