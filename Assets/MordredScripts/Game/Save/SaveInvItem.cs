using System.Collections.Generic;
using System.Collections;
using System.IO;

using Mordred.Game;

[System.Serializable]
public class SaveInvItem {

	public PrefabID prefab;
	public int quantity;
	public int invNr;
	public SlotType slotType;

	public SaveInvItem() {}

	public SaveInvItem(PrefabID prefab, int quantity, int invNr, SlotType slotType) {
		this.prefab = prefab;
		this.quantity = quantity;
		this.invNr = invNr;
		this.slotType = slotType;
	}
}


