using System.Collections.Generic;
using System.Collections;
using System.IO;

using Mordred.Game;

[System.Serializable]
public class SaveNpc {

	public PrefabID prefab;
	public bool dead; // To indicate permanent death
	public int health; // Last Health

	public Vec3 pos;
	public Vec3 rot;
	
	public SaveNpc() {}

	public SaveNpc(PrefabID prefab, bool dead, int health, Vec3 pos, Vec3 rot) {
		this.prefab = prefab;
		this.dead = dead;
		this.health = health;
		this.pos = pos;
		this.rot = rot;
	}
}


