using UnityEngine;
using System.Collections;

using Mordred.Game;
using Mordred.Extern;

namespace Mordred.Game.Environment
{

    [RequireComponent(typeof(SphereCollider))]
    [RequireComponent(typeof(Event))]
    public class MoveScript : MonoBehaviour
    {
        public float distance = 15F;

        float duration = 0.7F;

        private Vector3 startPoint;
        Vector3 endPoint;
        private float startTime;
        private Transform target;
        private Event eventType;

        void Start()
        {
            eventType = GetComponent<Event>();
            SphereCollider c = GetComponent<SphereCollider>();
            c.isTrigger = true;
            c.radius = distance;
            enabled = false;
        }

        void OnTriggerEnter(Collider c)
        {

            if (c.gameObject.tag == "Player")
            {
                //print("ENTER");
                ObjEnable(true);
            }
        }


        private void ObjEnable(bool b)
        {
            if (b)
            {
                target = GameObject.FindGameObjectWithTag("Player").transform;
                startPoint = transform.position;
                startTime = Time.time;
            }
            enabled = b;

        }

        void Update()
        {
            endPoint = target.position;
            //transform.position = Vector3.Lerp(startPoint, endPoint, Mathfx.Berp(0.1F, 1.0F, (Time.time - startTime) / duration));
            float timePassed = (Time.time - startTime);
            transform.position = Vector3.Lerp(startPoint, endPoint, (Time.time - startTime) / duration);
            
            if (timePassed > duration)
            {
                Destroy(gameObject);
                if (eventType != null)
                {
                    eventType.Execute();
                }
            }
        }
    }
}
