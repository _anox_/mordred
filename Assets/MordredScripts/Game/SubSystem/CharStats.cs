using UnityEngine;
using System.Collections.Generic;

using Mordred.Game;

namespace Mordred.Game.SubSystem
{

    public class CharStats : MonoBehaviour
    {

        public int Health { get; set; }
        public int MaxHealth { get; set; }

        public int Gold { get; set; }
        public int Silver { get; set; }
        public int Copper { get; set; }

        public int Ji { get; set; }
        public int Toh { get; set; }

        // TODO: Buffs, etc:
        // private Effekt[] effekt;


        public void AddCopper(int c)
        {
            Copper += c;
        }
    }
}
