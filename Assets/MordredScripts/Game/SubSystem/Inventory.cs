using UnityEngine;
using System.Collections.Generic;

using Mordred.User;
using Mordred.Obj;
using Mordred.Game.Handler;

namespace Mordred.Game.SubSystem
{
    public class Inventory
    {

        /**** Body slots  for quick reference****/
        public Dictionary<SlotType, WorldObject> slotDic /*lool*/ = new Dictionary<SlotType, WorldObject>();
        public WorldObject[] slots = new WorldObject[25];
        public int activSlot = 0;
        private Player player;

        public Inventory(Player p)
        {
            this.player = p;
        }

        public WorldObject GetActivItem()
        {
            return slots[activSlot];
        }

        public void AddToSlot(WorldObject worldObject)
        {
            SlotType st = worldObject.slotType;
            WorldObject slot = null;

            if (slotDic.TryGetValue(st, out slot) || slot == null)
            {// Fill empty slot!
                slotDic.Add(st, worldObject);
                Transform playerSlot = null;
                if (player.GetComponent<MountHandler>().mountDic.TryGetValue(st, out playerSlot) && playerSlot != null)
                {
                    worldObject.gameObject.SetActive(true);
                    worldObject.gameObject.transform.parent = playerSlot;
                    worldObject.gameObject.transform.rotation = player.transform.rotation;
                    worldObject.gameObject.transform.localPosition = Vector3.zero;

                }

            }
        }

        public void AddItemToInventory(WorldObject item, int targetSlot = -1)
        {
            //TODO: Do checks on weight, etc

            if (targetSlot == -1)
            {
                targetSlot = NextFreeSlot();
            }

            // Assign
            if (targetSlot >= 0)
            {
                Debug.Log("Adding Item to slot:" + targetSlot);
                slots[targetSlot] = item;
                item.gameObject.SetActive(false); // D A N G E R !! ! ! !  !

                // UpdateGui
                MenuHandler.Instance.UpdateInventory();

            }
            else
            {
                Debug.Log("No free slots found!");
            }

        }

        private int NextFreeSlot()
        {
            // Find empty slot
            for (var i = 0; i < slots.Length; i++)
            {
                if (slots[i] == null)
                {
                    return i;
                }
            }

            return -1;
        }

        public void SwitchSlots(int slot1, int slot2)
        {
            WorldObject wo_slot1 = slots[slot1];
            slots[slot1] = slots[slot2];
            slots[slot2] = wo_slot1;
            Debug.Log("s1" + slots[slot1] + " s2:" + slots[slot2]);
        }

        public void LoadSavedItems(SaveInvItem[] items)
        {
            // INIT Lists.....
            slotDic = new Dictionary<SlotType, WorldObject>();
            slots = new WorldObject[25];

            //Get load items
            foreach (SaveInvItem item in items)
            {

                GameObject itemPrefab = MordredUtils.Instance.InstantiateItem(item.prefab, Vector3.zero);
                if (item.slotType == SlotType.BAG)
                { // Into the bag
                    AddItemToInventory(itemPrefab.GetComponent<WorldObject>(), item.invNr);
                }
                else
                { // A Slot mounted item
                    AddToSlot(itemPrefab.GetComponent<WorldObject>());
                }
            }
        }

        public SaveInvItem[] SaveInvItems()
        {
            List<SaveInvItem> saveItems = new List<SaveInvItem>();
            //Get all slot items
            foreach (KeyValuePair<SlotType, WorldObject> pair in slotDic)
            {
                WorldObject wo = pair.Value;
                if (wo != null)
                {
                    saveItems.Add(new SaveInvItem(wo.prefabID, 1, -1, SlotType.NONE));
                }
            }

            //Get all bag items
            //TODO: Clear all bag sprites?
            for (var i = 0; i < slots.Length; i++)
            {
                WorldObject wo = slots[i];
                if (wo != null)
                {
                    saveItems.Add(new SaveInvItem(wo.prefabID, 1, i, wo.slotType));
                }
            }

            return saveItems.ToArray();//typeof(SaveInvItem)) as SaveInvItem[];
        }

    }
}
