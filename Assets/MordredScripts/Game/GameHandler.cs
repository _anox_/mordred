using UnityEngine;
using System.Collections;

using Mordred.Game.Environment;
using Mordred.Game.Handler;

namespace Mordred.Game
{
    public class GameHandler : MonoBehaviour
    {
        public static GameHandler instance;
        public GameHandler instance2;
        public GameState gameState = GameState.PLAY;
        public DayNightController dnc;
        public MenuHandler menuHandler;
        public NPCHandler npcHandler;
        public UseCodeHandler usecodeHandler;
        public WarHandler warHandler;
        public MordredUtils mordredUtils;

        public void Start()
        {
            instance = this;
            instance2 = this;
            DontDestroyOnLoad(gameObject);
            BootsTrap();
        }

        private void BootsTrap()
        {
            PlayerPrefs.DeleteAll();
            dnc = DayNightController.Instance;
            menuHandler = MenuHandler.Instance;
            npcHandler = NPCHandler.Instance;
            usecodeHandler = UseCodeHandler.Instance;
            warHandler = WarHandler.Instance;
            mordredUtils = MordredUtils.Instance;
        }

        public bool IsGameState(GameState state)
        {
            return state == gameState;
        }

    }
}
