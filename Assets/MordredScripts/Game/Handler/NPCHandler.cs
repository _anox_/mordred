using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Mordred.Obj.Char;

namespace Mordred.Game.Handler
{

    public class NPCHandler : MonoBehaviour
    {

        private static NPCHandler instance = null;

        public static NPCHandler Instance
        {
            get
            {
                if (instance == null)
                {
                    Debug.Log("Starte NPCHandler System.");
                    instance = GameObject.FindGameObjectWithTag("handler").AddComponent<NPCHandler>();
                }
                return instance;
            }
        }

        public Dictionary<int, Npc> npcMap = new Dictionary<int, Npc>();

        public void HandleNPC(Npc c)
        {
            Debug.Log("[NPCHandler] Handling NPC: " + c.name);
        }

        public void AddNpc(Npc c)
        {

            //TODO: Add NPC to custom GO

            if (c.gameObject.tag != "Player")
            {
                if (!npcMap.ContainsKey(c.GetInstanceID()))
                {
                    Debug.Log("[NPCHandler] Adding npc: " + c.name);
                    npcMap.Add(c.GetInstanceID(), c);
                    PrintNpcMap();
                }
            }
        }

        public void PrintNpcMap()
        {
            Debug.Log("========Npc Map=============");
            foreach (KeyValuePair<int, Npc> pair in npcMap)
            {
                Debug.Log("=> " + pair.Key + ": " + pair.Value.name);
            }
        }


        public void LoadSavedNPCs(SaveNpc[] items)
        {
            // INIT Lists.....
            npcMap.Clear();

            //Get load items
            foreach (SaveNpc item in items)
            {
                GameObject itemPrefab = MordredUtils.Instance.InstantiateNpc(item);
                AddNpc(itemPrefab.GetComponent<Npc>());
            }
        }

        public SaveNpc[] SaveNPCs()
        {
            List<SaveNpc> saveItems = new List<SaveNpc>();
            //Get all NPCs
            foreach (KeyValuePair<int, Npc> pair in npcMap)
            {
                Npc c = pair.Value;
                Vec3 pos = SaveTools.Vector3ToVec3(c.transform.position);
                Vec3 rot = SaveTools.Vector3ToVec3(c.transform.eulerAngles);

                if (c != null) { saveItems.Add(new SaveNpc(c.Prefab, false, c.Stats.Health, pos, rot)); }
                else
                {
                    throw new System.Exception("Konnte char nicht abspeichern");
                }
            }

            return saveItems.ToArray();
        }


    }

}

