using UnityEngine;
using System.Collections;

using Mordred.Obj;
using Mordred.Obj.Item;
using Mordred.User;
using Mordred.Game;

namespace Mordred.Game.Handler
{

    public class WarHandler : MonoBehaviour
    {


        private Player player;
        private static WarHandler instance = null;

        public static WarHandler Instance
        {
            get
            {
                if (instance == null)
                {
                    Debug.Log("Starte WarHandler System.");
                    instance = GameObject.FindGameObjectWithTag("handler").AddComponent<WarHandler>();
                    instance.player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
                }
                return instance;
            }
        }

	
	public void TryAttack() {
		// Player will undefiniert angreifen	
		Debug.Log("Hitting....");
		/*TODO: 
			-> Waffe und Modus identifizieren
		
	

		*/
	}

        public void Attack(WorldObject target)
        {
            Debug.Log("Attacking");
            WorldObject weapon = player.Inventory.GetActivItem();
            if (weapon != null && weapon.type == ObjectType.WEAPON)
            {
                Debug.Log("is WEapon");
                target.health -= ((Weapon)weapon).baseDamage;
                Debug.Log("health:"+target.health);
                if (target.health <= 0 && target.isDestroyable)
                {
                    Debug.Log("target dead");
                    if (target.lootType == LootType.GOLD_10)
                    {
                        GameObject gold = MordredUtils.Instance.InstantiateItem(PrefabID.GOLD_PILE, target.transform.position);
                        GameObject.Destroy(gold, 300F); // Timeout destroy after 5 minutes
                        //gold.GetComponent<
                    }

                    GameObject.Destroy(target.gameObject);

                }
            }
        }

        public void HandleEvent(Event e)
        {
            Debug.Log("STUB..... Handling event Damage");
        }
    }
}
	
