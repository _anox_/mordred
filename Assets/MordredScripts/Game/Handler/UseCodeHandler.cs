using UnityEngine;
using System.Collections;

using Mordred.User;

using Mordred.Obj;
using Mordred.Obj.Char;
using Mordred.Game;
namespace Mordred.Game.Handler
{

    public class UseCodeHandler : MonoBehaviour
    {

        private static UseCodeHandler instance = null;

        public static UseCodeHandler Instance
        {
            get
            {
                if (instance == null)
                {
                    Debug.Log("Starte UseCodeHandler System.");
                    instance = GameObject.FindGameObjectWithTag("handler").AddComponent<UseCodeHandler>();
                }
                return instance;
            }
        }

        private Player player;

        public void Awake()
        {
            DontDestroyOnLoad(gameObject);

            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

        }

	public void Handle(GameObject go) 
	{
                WorldObject wo = go.GetComponent<WorldObject>();
                Npc npc = go.GetComponent<Npc>();
                if (wo != null)
                {
                    Debug.Log("Item Geklickt!");
                    HandleItem(wo);
                }
                else if (npc != null)
                {
                    Debug.Log("Npc geklickt"); // Feindliche NPCs sind nicht usable
                    NPCHandler.Instance.HandleNPC(npc);
                }
                else 
		{
		    Debug.LogWarning("Usable Object geklickt ohne dass dies ein WO oder C ist..."); 
		}
	}


        public void HandleItem(WorldObject item)
        {
            // Erstmal direkt aufnehmen, ins inventar und an den rightHandMount 
            if (item.isDestroyable && item.health > 0)
            {
                WarHandler.Instance.Attack(item);
            }
            else
            {
                //checked what to do with item
                player.Inventory.AddItemToInventory(item, -1);
            }
        }



        public void HandleEvent(Event e)
        {
            if (e.eventType == Event.EventType.LOOT)
            {
                switch (e.loot)
                {
                    case LootType.GOLD_10: player.Character.Stats.AddCopper(10); break;
                }
            }
        }
    }
}
