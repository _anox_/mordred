using UnityEngine;
using System.Collections;

using Mordred.Gui;

namespace Mordred.Game.Handler
{

    public class MenuHandler : MonoBehaviour
    {

        private static MenuHandler instance = null;

        public static MenuHandler Instance
        {
            get
            {
                if (instance == null)
                {
                    Debug.Log("Starte MenuHandler System.");
                    instance = GameObject.FindGameObjectWithTag("handler").AddComponent<MenuHandler>();
                }
                return instance;
            }
        }

        public BaseMenu activeMenu;
        public SaveMenu saveMenu;
        //public InvGui inventory;


        public void Start()
        {
            DontDestroyOnLoad(gameObject);

            //saveMenu = (SaveMenu)GameObject.FindGameObjectWithTag("savemenu").GetComponent<SaveMenu>();
            //inventory = (InvGui)GameObject.FindGameObjectWithTag("inventory").GetComponent<InvGui>();
        }


        /**
        * Wenn Kein Menu Activ -> PauseMenu
        * WEnn Menu activ -> hide
        */
        public void ToggleMenu()
        {
            if (activeMenu != null)
            {
                activeMenu.Show(false);
            }
            else
            {
                ShowMenu(MenuType.PAUSE);
            }
        }

        public void ShowMenu(MenuType menuType)
        {

            if (activeMenu != null && activeMenu.guiActive) { Debug.Log("Cant display Menu. Another is still active!"); return; }
            BaseMenu selectedMenu = null;

            switch (menuType)
            {

                case MenuType.MAIN: break;
                case MenuType.SAVELOAD: selectedMenu = saveMenu; break;
                //case MenuType.INVENTORY: selectedMenu = inventory; break;

            }
            activeMenu = selectedMenu;
            activeMenu.Show(true);
        }

        public void UpdateInventory()
        {
            /*if(activeMenu==inventory) {
                inventory.UpdateInventory();
            }*/
        }

    }
}
