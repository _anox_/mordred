namespace Mordred.Game {

	public enum MenuType { MAIN, PAUSE, SAVELOAD, INVENTORY }
	public enum GameState { PLAY, MENU}

	public enum ObjectType { ITEM, WEAPON }
	public enum WeaponType { MELEE, PROJECTILE, REMOTE }
	public enum DamageType { BASIC, MACE, SWORD, HAMMER }
	
	public enum PrefabID { PITCHFORK, GOLD_PILE }
	public enum SkillType { SWORDSMANSHIP, MACE }
	public enum SlotType { RIGHTHAND=0, LEFTHAND=1, BOTHANDS=2, ANYHAND=3, HEAD=4, TORSO=5, LEGS=6, FEET=7, BAG=8, NONE=9 }

	public enum LootType { NONE, GOLD_10, GOLD_100, GOLD_1000, SMALL_LOOT, MED_LOOT, BIG_LOOT }
}
