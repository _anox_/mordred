using UnityEngine;
using System.Collections;

using Mordred.User;
using Mordred.Obj;
//using Mordred.Game.SubSystem;

namespace Mordred.Game.Dev
{

    public class DebugGUI : MonoBehaviour
    {
        private Player player;
        void OnGUI()
        {
            string stats = collectStatData();
            // Make a background box
            GUI.Box(new Rect(10, 20, 200, 200), stats);
        }

        private string collectStatData()
        {
            if (player == null)
            {
                player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

            }

            string data = string.Format("name: {0}\nhour :{1}\nactiveItem: {2}\ng:{3} s:{4} c:{5}", 
                player.Character.CharName, 
                GameHandler.instance.dnc.worldTimeHour, 
                player.Inventory.GetActivItem() != null ? player.Inventory.GetActivItem().objectName : "None",
                player.Character.Stats.Gold, player.Character.Stats.Silver, player.Character.Stats.Copper);

            return data;
        }
    }

}
