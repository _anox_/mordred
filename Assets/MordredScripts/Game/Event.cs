using UnityEngine;
using System.Collections;

using Mordred.Obj.Char;
using Mordred.Game.Handler;

namespace Mordred.Game
{

    public class Event : MonoBehaviour
    {
        public enum EventType { LOOT, DAMAGE }
        public EventType eventType;
        public LootType loot;
        public Character origin { get; set; }

        public void Execute()
        {
            switch (eventType)
            {
                case EventType.LOOT: UseCodeHandler.Instance.HandleEvent(this); break;
                case EventType.DAMAGE: WarHandler.Instance.HandleEvent(this); break;
            }

        }


    }

}