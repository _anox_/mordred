using Mordred.Game;
using Mordred.User;
using UnityEngine;

namespace Mordred.Obj.Item
{

    public class Weapon : WorldObject
    {

        public WeaponType weaponType;
        public DamageType damageType;

        public int distance;
        public int baseDamage;

        public Animation castAnimation;

        public virtual void PerformAction(string actionToPerform)
        {
            //it is up to children with specific actions to determine what to do with each of those actions
        }

        public virtual void MouseClick(GameObject hitObject, Vector3 hitPoint, Player controller)
        {
        }


    }

}