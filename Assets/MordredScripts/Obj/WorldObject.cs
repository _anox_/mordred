using UnityEngine;
using System.Collections;

using Mordred.Game;
using Mordred.User;

namespace Mordred.Obj
{

    public class WorldObject : MonoBehaviour
    {

        // Nuetzlich fuer Quests, schluessel, etc
        public int objId;

        public string objectName;
        public string spriteName;

        public PrefabID prefabID;

        public ObjectType type;

        public SlotType slotType = SlotType.ANYHAND;
	    public LootType lootType = LootType.NONE;
 
        public bool isDestroyable = false;
        public int health = 100;
	        
        public int cost;


        public void Start()
        {
            gameObject.layer = LayerMask.NameToLayer("usable");
        }

    }
}
