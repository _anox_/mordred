using UnityEngine;
using System.Collections;

using Mordred.Game;
using Mordred.Game.SubSystem;
using Mordred.Game.Handler;
using Mordred.Obj.Item;

namespace Mordred.Obj.Char
{

    /*
     * Alles was auf Chars zutrifft.
     * Player erbt diese Eigenschaft.
     * NPC spezifisches kommt in die Npc Klasse.
     */
    [RequireComponent(typeof(CharStats))]
    [RequireComponent(typeof(SkillBook))]
    public class Character : MonoBehaviour
    {

        public string CharName { get; set;}
	    public CharStats Stats {get; private set;}
        public SkillBook Skills {get; private set;}

        public void Start()
        {
		this.Skills = GetComponent<SkillBook>();
		this.Stats = GetComponent<CharStats>();	    
        }

    }
}
