using UnityEngine;
using System.Collections;

using Mordred.Game;
using Mordred.Game.SubSystem;
using Mordred.Game.Handler;
using Mordred.Obj.Item;

namespace Mordred.Obj.Char
{

    /*
     * Alles was auf Chars zutrifft.
     * Player erbt diese Eigenschaft.
     * NPC spezifisches kommt in die Npc Klasse.
     */
    [RequireComponent(typeof(AreaTrigger))]
    public class Npc : Character
    {

        public PrefabID Prefab {get;set;}
        public bool Talkable {get;set;}
        public Weapon Weapon {get;set;}

        //TO NPC TODO: Loot, dead, converse, aggro-style
        // TODO: Groessere Sachen ueber RequireCOmponent einbinden plz

        public new void Start()
        {
	        base.Start();
            NPCHandler.Instance.AddNpc(this);
        }

    }
}
