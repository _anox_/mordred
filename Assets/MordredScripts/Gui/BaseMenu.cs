using UnityEngine;
using System.Collections;

namespace Mordred.Gui
{
    public abstract class BaseMenu : MonoBehaviour
    {

        public void Start()
        {
            guiActive = false;
            DontDestroyOnLoad(gameObject);
        }


        public bool guiActive = false;

        public void Show(bool activ)
        {
            Debug.Log("Active=" + activ + " gui:" + guiActive);
            if (guiActive == activ) return;

            Debug.Log("Active=" + activ);
            guiActive = activ;

            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(guiActive);
            }
            if (guiActive)
            {
                InitMenu();
            }
        }

        public abstract void InitMenu();

    }
}