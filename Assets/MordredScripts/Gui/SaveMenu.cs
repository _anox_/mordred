using UnityEngine;
using System.Collections;

using Mordred.Game.Save;

namespace Mordred.Gui
{
    public class SaveMenu : BaseMenu
    {

        public UILabel[] labelList;
        public int currentIdx;


        public override void InitMenu()
        {
            Debug.Log("[SaveMenu] Init() Active=" + guiActive);
            SetButtonLabels();
            currentIdx = -1;
        }

        private void SetButtonLabels()
        {
            for (int i = 0; i < labelList.Length; i++)
            {
                GameData gd = null;
                SaveLoad.GetSaveGames().TryGetValue(i, out gd);
                labelList[i].text = (gd != null ? gd.saveDate.ToString() : "EMPTY");
            }
        }

        public void MarkGameIndex(int idx)
        {
            currentIdx = idx;
        }

        public void Save()
        {
            SaveLoad.SaveGameData(currentIdx);
            Show(false);
        }

        public void Load()
        {
            SaveLoad.LoadGameDataFromCache(currentIdx);
            Show(false);
        }

    }
}
